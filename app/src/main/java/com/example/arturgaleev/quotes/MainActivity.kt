package com.example.arturgaleev.quotes

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import okio.ByteString
import java.util.concurrent.TimeUnit
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet


class MainActivity : AppCompatActivity() , AdapterView.OnItemSelectedListener{

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.d ("Выбрано","Selected : "+list_quote[position])
    }

    private lateinit var webSocket: WebSocket
    private lateinit var wsListener: WebSocketListener
    val entries = ArrayList<Entry>()
    val entries_1 = ArrayList<Entry>()
    val list_quote = arrayOf("BTCUSD", "EURUSD")
    var url_API = "wss://api.bitfinex.com/ws"
    var i = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        spinner_quotes.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_quote)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_quotes.adapter = aa
    }

    override fun onStop() {
        super.onStop()
        webSocket.close(1000, "Close")
    }

    override fun onStart() {
        super.onStart()
        val client = OkHttpClient.Builder()
            .readTimeout(2, TimeUnit.SECONDS)
            .build()
        val request = Request.Builder()
            .url(url_API)
            .build()

        wsListener = object : WebSocketListener() {

            override fun onOpen(webSocket: WebSocket, response: Response) {
                super.onOpen(webSocket, response)
                webSocket.send("{\n" +
                        "   \"event\":\"subscribe\",\n" +
                        "   \"channel\":\"ticker\",\n" +
                        "   \"pair\":\"BTCUSD\"\n" +
                        "}")
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                output(text)
                if (!text.contains("event")){
                    val a = text.substring (1, text.indexOf("]"))
                    val array = a.split(",")
                    if (!array[1].contains("hb"))
                    {
                        val text2 = array[1].toFloat()
                        val text3 = array[3].toFloat()
                        output(text2.toString())
                        output(text3.toString())
                        entries.add(Entry( i++.toFloat(), text2))
                        entries_1.add(Entry(i++.toFloat(),text3))

                        val dataSets = ArrayList<ILineDataSet>()

                        val dataSet = LineDataSet(entries, "BTCUSD_Bid")
                        dataSet.color = Color.GREEN
                        dataSet.valueTextColor = Color.GREEN
                        val dataSet1 = LineDataSet(entries_1, "BTCUSD_Ask")
                        dataSet1.color = Color.RED
                        dataSet1.valueTextColor = Color.RED

                        dataSets.add(dataSet)
                        dataSets.add(dataSet1)

                        chart.data = LineData(dataSets)

                        chart.invalidate()
                    }
                    else
                    {
                        chart.invalidate()
                    }
                }
            }

            override fun onMessage(webSocket: WebSocket?, bytes: ByteString?) {
                super.onMessage(webSocket,bytes)
                output("Receiving bytes : " + bytes!!.hex())
            }

            override fun onClosing(webSocket: WebSocket?, code: Int, reason: String?) {
                super.onClosing(webSocket,code,reason)
                webSocket!!.close(1000, null)
                output("Closing : $code / $reason")
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: okhttp3.Response?) {
                super.onFailure(webSocket,t,response)
                output("Error : " + t.message)
            }

            private fun output(txt: String) {
                Log.v("WSS", txt)
            }
        }
        webSocket = client.newWebSocket(request, wsListener)
    }
}

